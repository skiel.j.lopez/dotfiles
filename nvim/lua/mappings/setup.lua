-- Set leader to be ','
vim.g.mapleader = ','

-- Get back to normal mode with 'jj'
vim.keymap.set('i', 'jj', '<ESC>', { remap = false })

-- Accept Copilot suggestions
vim.keymap.set('i', '<C-j>', 'copilot#Accept("<CR>")', { noremap = true, silent = true, expr = true, replace_keycodes = false })

-- Easy window navigation
vim.keymap.set('n', '<C-h>', '<C-w>h', { remap = false })
vim.keymap.set('n', '<C-j>', '<C-w>j', { remap = false })
vim.keymap.set('n', '<C-k>', '<C-w>k', { remap = false })
vim.keymap.set('n', '<C-l>', '<C-w>l', { remap = false })

-- Easy split window
vim.keymap.set('n', '<leader>H', ':botright new<CR>', { remap = false, silent = true })
vim.keymap.set('n', '<leader>V', ':botright vnew<CR>', { remap = false, silent = true })

-- Tab navigation
vim.keymap.set('n', 'th', ':tabfirst<CR>', { remap = false, silent = true })
vim.keymap.set('n', 'tj', ':tabprev<CR>', { remap = false, silent = true })
vim.keymap.set('n', 'tk', ':tabnext<CR>', { remap = false, silent = true })
vim.keymap.set('n', 'tl', ':tablast<CR>', { remap = false, silent = true })
vim.keymap.set('n', 'tt', ':tabnew<CR>', { remap = false, silent = true })

-- Buffer navigation
-- vim.keymap.set('n', '<S-H>', ':bp<CR>', { remap = false, silent = true })
-- vim.keymap.set('n', '<S-L>', ':bn<CR>', { remap = false, silent = true })
-- vim.keymap.set('n', '<S-x>', ':bp<bar>sp<bar>bn<bar>bd<CR>', { remap = false, silent = true })
vim.keymap.set('n', '<S-H>', ':TablineBufferPrevious<CR>', { remap = false, silent = true })
vim.keymap.set('n', '<S-L>', ':TablineBufferNext<CR>', { remap = false, silent = true })
vim.keymap.set('n', '<S-T>', ':TablineToggleShowAllBuffers<CR>', { remap = false, silent = true })
vim.keymap.set('n', '<S-x>', ':bp<bar>sp<bar>bn<bar>bd<CR>', { remap = false, silent = true })

-- Paste in a new line
vim.keymap.set('n', '<C-p>', 'o <C-r>"<ESC>', { remap = false })

-- Ctrl-n to disable search match hightlight
vim.keymap.set('n', '<C-n>', ':silent noh<CR>', { remap = false, silent = true })

-- Open Fiel Explorer
-- vim.keymap.set('n', '<leader>e', ':Neotree action=focus toggle=true position=left<CR>', { remap = false, silent = true })

-- Pounce fancy search
--vim.keymap.set({ 'n', 'v' }, 'f', ':Pounce<CR>', { remap = false, silent = true })
--vim.keymap.set('n', 'F', ':PounceRepeat<CR>', { remap = false, silent = true })

-- Awesome search plugin
-- Find a file in the current dir by name
vim.keymap.set({ 'n', 'v' }, '<leader>ff', ':Telescope find_files<CR>', { remap = false, silent = true })
-- Fing a file with the provided string in the current directory
vim.keymap.set({ 'n', 'v' }, '<leader>fg', ':Telescope live_grep<CR>', { remap = false, silent = true })
-- List and find a git file by name (modified files listed to commit in git)
vim.keymap.set({ 'n', 'v' }, '<leader>gf', ':Telescope git_files<CR>', { remap = false, silent = true })
-- List git branches and check out
vim.keymap.set({ 'n', 'v' }, '<leader>gb', ':Telescope git_branches<CR>', { remap = false, silent = true })

-- Use TAB to move throught suggestions
vim.keymap.set('i', '<Tab>', function()
  return vim.fn.pumvisible() == 1 and '<C-n>' or '<Tab>'
end, { expr = true })

vim.keymap.set('i', '<S-Tab>', function()
  return vim.fn.pumvisible() == 1 and '<C-p>' or '<S-Tab>'
end, { expr = true })
