-- Compile on plugins.lua write
local packer_group = vim.api.nvim_create_augroup('Packer', { clear = true })
vim.api.nvim_create_autocmd(
  'BufWritePost',
  { command = "source <afile> | PackerCompile", group = packer_group, pattern = 'plugins.lua' }
)

-- Install packer
local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end

  return false
end

local packer_bootstrap = ensure_packer()

return require('packer').startup({
  function(use)
    -- Packer can manage itself
    use 'wbthomason/packer.nvim'

    -- Gruvbox colors
    -- use 'ellisonleao/gruvbox.nvim'

    -- Nightfox colors
    use 'EdenEast/nightfox.nvim'

    -- LSP configuration
    use {
      'williamboman/mason.nvim',
      config = require('mason').setup {},
    }

    use {
      'williamboman/mason-lspconfig.nvim',
      config = require('mason-lspconfig').setup {},
    }

    use {
      'neovim/nvim-lspconfig',
      config = function() require('configs.lsp') end,
    }

    -- Autocompletion
    use({
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-buffer',
      'hrsh7th/cmp-path',
      'hrsh7th/cmp-cmdline',
      'hrsh7th/nvim-cmp',
      'hrsh7th/cmp-vsnip',
      'hrsh7th/vim-vsnip',
      config = require('configs.cmp'),
    })

    -- LSP Saga
    use {
      'nvimdev/lspsaga.nvim',
      after = 'nvim-lspconfig',
      config = function () require('configs.lspsaga') end,
      requires = {
        'nvim-treesitter/nvim-treesitter',
        'kyazdani42/nvim-web-devicons',
      },
    }

    -- Signature help
    use {
      "ray-x/lsp_signature.nvim",
      config = function() require('lsp_signature').setup() end,
    }

    -- Neotree
    use {
      'nvim-neo-tree/neo-tree.nvim',
      branch = 'v3.x',
      requires = {
        'nvim-lua/plenary.nvim',
        'kyazdani42/nvim-web-devicons',
        'MunifTanjim/nui.nvim',
        {
          's1n7ax/nvim-window-picker',
          version = '2.*',
          config = function()
            require 'window-picker'.setup({
              filter_rules = {
                include_current_win = false,
                autoselect_one = true,
                -- filter using buffer options
                bo = {
                  -- if the file type is one of following, the window will be ignored
                  filetype = { 'neo-tree', "neo-tree-popup", "notify" },
                  -- if the buffer type is one of following, the window will be ignored
                  buftype = { 'terminal', "quickfix" },
                },
            },
          })
          end,
        },
      },
      config = function ()
        -- If you want icons for diagnostic errors, you'll need to define them somewhere:
        vim.fn.sign_define("DiagnosticSignError",
          {text = " ", texthl = "DiagnosticSignError"})
        vim.fn.sign_define("DiagnosticSignWarn",
          {text = " ", texthl = "DiagnosticSignWarn"})
        vim.fn.sign_define("DiagnosticSignInfo",
          {text = " ", texthl = "DiagnosticSignInfo"})
        vim.fn.sign_define("DiagnosticSignHint",
          {text = "󰌵", texthl = "DiagnosticSignHint"})

        require('configs.neotree')

        vim.keymap.set('n', '<leader>e', ':Neotree toggle<CR>', { remap = false, silent = true })
      end,
    }

    -- Buffers in tabline
    use {
      'kdheepak/tabline.nvim',
      requires = {
        { 'hoob3rt/lualine.nvim', opt=true },
        { 'kyazdani42/nvim-web-devicons', opt = true }
      },
      config = function() require('configs.tabline') end,
    }

    -- Status line
    use {
      'nvim-lualine/lualine.nvim',
      config = function() require('configs.lualine') end,
      event = 'VimEnter',
      requires = { 'kyazdani42/nvim-web-devicons', opt = true },
    }

    -- Preview hex colors
    use {
      'norcalli/nvim-colorizer.lua',
      event = 'BufReadPre',
      config = function() require('configs.colorizer') end,
    }

    -- Advanced syntax highlighting
    use {
      'nvim-treesitter/nvim-treesitter',
      config = function() require('configs.treesitter') end,
      run = ':TSUpdate',
    }

    -- Syntax highlight in text-objects
    use({ 'nvim-treesitter/nvim-treesitter-textobjects' })

    -- Git lense functionality
    use {
      'lewis6991/gitsigns.nvim',
      requires = { 'nvim-lua/plenary.nvim' },
      event = 'BufReadPre',
      config = function() require('configs.gitsigns') end,
    }

    -- Comment toggler
    use {
      'numToStr/Comment.nvim',
      config = function() require('Comment').setup() end,
    }

    -- Fuzzy search files across project
    use {
      'nvim-telescope/telescope.nvim',
      module = 'telescope',
      cmd = 'Telescope',
      requires = {
        { 'nvim-lua/plenary.nvim' },
        { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make' },
        { 'BurntSushi/ripgrep' },
      },
      config = function() require('configs.telescope') end,
    }

    use 'nvim-telescope/telescope-file-browser.nvim'

    -- Indentation lines
    use {
      'lukas-reineke/indent-blankline.nvim',
      config = function() require('configs.indentline') end,
    }

    -- Github Copilot AI pairprogrammer
    use { 'github/copilot.vim', branch = 'release' }

    -- Git Diff with merge tool
    use { 'sindrets/diffview.nvim' }

    -- Sync if the function exists
    if packer_bootstrap then
      require('packer').sync()
    end
  end,
  config = {
    display = { open_fn = require('packer.util').float },
    profile = { enable = true, threshold = 1 }
  }
})
