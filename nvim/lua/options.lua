-- Term GUI colors
vim.opt.termguicolors = true
-- Show numbers
vim.opt.number = true
-- Delete with backspace
vim.opt.backspace = { "indent", "eol", "start" }
-- Use system clipboard
vim.opt.clipboard = "unnamedplus"
-- Set default encoding to UTF-8
vim.opt.encoding = "utf-8"
vim.opt.fileencoding = "utf-8"
-- Turn on syntax highlighting
vim.opt.syntax = "on"
-- Set UNIX line endings
vim.opt.fileformat = "unix"
-- When reading files try UNIX line then DOS. Use UNIX for new buffers
vim.opt.fileformats = "unix,dos"
-- I don't know what this does
vim.opt.completeopt = "menu,menuone,noselect"
-- Show cursor line
vim.opt.cursorline = false
-- Show cursor column
vim.opt.cursorcolumn = false

-- Enable background buffers
vim.opt.hidden = true
-- Highlight found searches
vim.opt.hlsearch = true
-- Ignore case
vim.opt.ignorecase = true
-- Get a preview of replacements
vim.opt.inccommand = "split"
-- Show match white typing
vim.opt.incsearch = true
-- No double spaces with join
vim.opt.joinspaces = false
vim.o.lazyredraw = true
-- No wrap lines
vim.opt.wrap = false

-- Show some invisible characters
vim.opt.list = true
vim.opt.listchars = { tab = " ", trail = "·" }
-- Show N lines above and below the cursor
vim.opt.scrolloff = 5

-- Insert indents automatically
vim.opt.smartindent = true
-- Round indent to multiple of shiftwidth
vim.opt.shiftround = true
-- Indent size (in spaces)
vim.opt.shiftwidth = 4
-- Copy indent from current line
vim.opt.autoindent = true

-- Don't show mode
vim.opt.showmode = false
-- Show N columns before and after the cursor
vim.opt.sidescrolloff = 10
-- Always show sign column
vim.opt.signcolumn = "number"
-- Don't ignore case with capitals
vim.opt.smartcase = true
-- Set language to have some spelling help
vim.opt.spelllang = { "en_us" }
-- Put new window below current
vim.opt.splitbelow = true
-- Put new window right of current
vim.opt.splitright = true
-- TAB size in spaces
vim.opt.tabstop = 4

-- New file type detection
-- vim.g.do_filetype_lua = 1
-- Disable filetype.vim
-- vim.g.did_load_filetypes = 0
-- Enable mouse support
vim.opt.mouse = "a"

-- Reload files after update via git
vim.opt.autoread = true
-- Reload files after externally modified
vim.o.autoread = true

-- Change tab to spaces
vim.opt.expandtab = true
vim.opt.foldenable = false
vim.opt.foldmethod = "indent"
vim.opt.formatoptions = "l"

-- Github Copilot options
vim.g.copilot_no_tab_map = true
vim.g.copilot_filetypes = {
  ["*"] = false,
  ["javascript"] = true,
  ["javascriptreact"] = true,
  ["typescript"] = true,
  ["typescriptreact"] = true,
  ["lua"] = true,
  ["python"] = true,
  ["rust"] = true,
  ["bash"] = true,
}

-- Set tab size to 2 by file type
local twoSpacesIndent = vim.api.nvim_create_augroup("TwoSpacesIndent", { clear = true })
vim.api.nvim_create_autocmd(
  { "BufNewFile", "BufRead" },
  {
    pattern = { "*.lua", "*.css", "*.scss", "*.js", "*.jsx", "*.ts", "*.tsx", "*.yaml", "*.yml" },
    command = "setlocal sts=2 ts=2 sw=2 et",
    group   = twoSpacesIndent
  }
)

-- Help NVIM to set the right file type
local fileTypes = vim.api.nvim_create_augroup("FileTypes", { clear = true })
vim.api.nvim_create_autocmd(
  { "BufNewFile", "BufRead" },
  {
    pattern = { "*.twig", "*.jinja2" },
    command = "setlocal ft=html",
    group = fileTypes
  }
)

vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(
  vim.lsp.handlers.signature_help,
  { border = "single" }
)

-- Set update time to 800ms
vim.opt.updatetime = 800
-- Show diagnostics in Normal mode and signature help in insert mode
local lspFloat = vim.api.nvim_create_augroup("LspFloat", { clear = true })
-- show diagnostics in normal mode
vim.api.nvim_create_autocmd(
  { "CursorHold" },
  {
    pattern = { "*" },
    command = "lua vim.diagnostic.open_float(0, { scope = 'line' })",
    group = lspFloat
  }
)
