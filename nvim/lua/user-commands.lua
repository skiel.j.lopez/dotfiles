-- Format JSON
vim.api.nvim_create_user_command(
  'FormatJSON',
  '%!python3 -m json.tool',
  { bang = true, desc = 'Format a json object' }
)
