require('lspsaga').setup {}
local map = vim.keymap.set

map({'n','v'}, 'ga', '<CMD>Lspsaga code_action<CR>', { noremap = true, silent = true })
map('n', 'K',  '<CMD>Lspsaga hover_doc<CR>', { noremap = true, silent = true })
map('i', '<C-k>', '<CMD>Lspsaga signature_help<CR>', { noremap = true, silent = true })
map('n', 'gl', '<CMD>Lspsaga show_line_diagnostics<CR>', { noremap = true, silent = true })
map('n', 'gb', '<CMD>Lspsaga show_buf_diagnostics<CR>', { noremap = true, silent = true })
map('n', 'gp', '<CMD>Lspsaga peek_definition<CR>', { noremap = true, silent = true })
map('n', 'gd', '<CMD>Lspsaga goto_definition<CR>', { noremap = true, silent = true })
map('n', 'gf', '<CMD>Lspsaga finder<CR>', { noremap = true, silent = true })
map('n', 'gr', '<CMD>Lspsaga rename<CR>', { noremap = true, silent = true })
map('n', 'gt', '<CMD>Lspsaga term_toggle<CR>', { noremap = true, silent = true })
map('n', 'go', '<cmd>Lspsaga outline<CR>', { noremap = true, silent = true })
map('n', '<leader>oc', '<cmd>Lspsaga outgoing_calls<CR>', { noremap = true, silent = true })
map('n', '<leader>ic', '<cmd>Lspsaga incoming_calls<CR>', { noremap = true, silent = true })
