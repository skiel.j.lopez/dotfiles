require('colorizer').setup({
  '*'; -- Highlight all files, but customize the following
  html = { names = false } -- Disable parsing "names" like Blue or Red
})
