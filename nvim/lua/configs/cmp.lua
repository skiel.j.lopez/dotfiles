local capabilities = require('cmp_nvim_lsp').default_capabilities()
local lsp = require('lspconfig')

local servers = {
  'lua_ls',
  'cssls',
  'bashls',
  'eslint',
  'pyright',
  'pylsp',
  'tsserver',
}

for _, lsp_name in ipairs(servers) do
  lsp[lsp_name].setup {
    capabilities = capabilities,
  }
end

local cmp = require('cmp')
cmp.setup {
  snippet = {
    expand = function(args)
      vim.fn['vsnip#anonymous'](args.body)
    end,
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-w>'] = cmp.mapping.scroll_docs(-4), -- Up
    ['<C-s>'] = cmp.mapping.scroll_docs(4), -- Down
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<CR>'] = cmp.mapping.confirm({
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    }),
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      else
        fallback()
      end
    end, { 'i', 's' }),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      else
        fallback()
      end
    end, { 'i', 's' }),
  }),
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
    { name = 'vsnip' },
    { name = 'nvim_lsp_signature_help' },
  }, {
    { name = 'buffer' },
  }),
}
