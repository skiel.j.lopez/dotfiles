require('neotree')

return require('packer').startup(function()
    use 'wbthomason/packer.nvim'

    use {neotree_conf}
end)
