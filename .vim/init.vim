"" USABILITY CONFIGURATION ====================================

" Term Gui Colors
if (has('termguicolors'))
  set termguicolors
endif

" don't make vim compatible with vi
set nocompatible

" turn on syntax highlighting
syntax enable

" and show line numbers
set number

" easy regex
set magic

" reload files changed outside vim
set autoread

" encoding is utf 8
set encoding=utf-8
set fileencoding=utf-8

" FiraMono font
"set guifont=FiraMono-Regular:h14

" enable matchit plugin which ships with vim and greatly enhances '%'
runtime macros/matchit.vim

" by default, in insert mode backspace won't delete over line breaks, or
" automatically-inserted indentation, let's change that
set backspace=indent,eol,start

" dont't unload buffers when they are abandoned, instead stay in the
" background
" set hidden

" set unix line endings
set fileformat=unix

" when reading files try unix line endings then dos, also use unix for new
" buffers
set fileformats=unix,dos

" save up to 100 marks, enable capital marks
set viminfo='100,f1

" screen will not be redrawn while running macros, registers or other
" non-typed comments
set lazyredraw

" no wrap lines, just continue to the right
set nowrap

" indentation
set expandtab       " use spaces instead of tabs
set autoindent      " autoindent based on line above, works most of the time
set smartindent     " smarter indent for C-like languages
set shiftwidth=4    " when reading, tabs are 4 spaces
set softtabstop=4   " in insert mode, tabs are 4 spaces

" make vim try to detect file types and load plugins for them
" ruby/css/javascript
" commands
" au: autocmd
" sts: softtabstop (how many spaces use when tab is presed)
" ts: how to display tabs
" sw: shiftwidth how many spaces per indent level
" et: expandtab use speces instead of tabs
au BufNewFile,BufRead *.rb   setlocal sts=2 ts=2 sw=2 et
au BufNewFile,BufRead *.css  setlocal sts=2 ts=2 sw=2 et
au BufNewFile,BufRead *.js   setlocal sts=2 ts=2 sw=2 et
au BufNewFile,BufRead *.json setlocal sts=2 ts=2 sw=2 et
au BufNewFile,BufRead *.ts   setlocal sts=2 ts=2 sw=2 et
au BufNewFile,BufRead *.tsx  setlocal sts=2 ts=2 sw=2 et
au BufNewFile,BufRead *.scss setlocal sts=2 ts=2 sw=2 et
au BufNewFile,BufRead *.yaml setlocal sts=2 ts=2 sw=2 et
au BufNewFile,BufRead *.yml  setlocal sts=2 ts=2 sw=2 et

" Allow comments in jsonc files
au FileType json syntax match Comment +\/\/.\+$+

" highlight 121 column
highlight ColorColumn ctermbg=magenta
call matchadd('ColorColumn', '\%121v', 100)

" highlight search results
set hlsearch
" show result while searching
set incsearch

" folding settings
set foldmethod=indent   "fold based on indent
set foldnestmax=10      "deepest fold is 10 levels
set nofoldenable
set foldlevel=1

"" COLORS SETTINGS ============================================

" Highlight trailing spaces.
:autocmd ColorScheme * highlight ExtraWhitespace ctermbg=darkred guibg=darkred

" Select colormap: 'soft', 'softlight', 'standard' or 'allblue'
"let xterm16_colormap = 'allblue'

" Select brightness: 'low', 'med', 'high', 'default' or custom levels.
"let xterm16_brightness = 'med'

"colo xterm16
"set background=dark
"colo jellybeans
"colo monokai
"colo muon
"colo neonwave
"colo sourcerer
"colo wellsokai
"colorscheme moonshine
"colorscheme Base2Tone_EveningDark
"colorscheme nord
" Select theme 'default' | 'palenight' | 'ocean' | 'lighter' | 'darker'
let g:material_theme_style = 'ocean'
let g:material_terminal_italics = 1
colorscheme material
"colorscheme codedark

" set transparent brackground
hi Normal ctermbg=none
hi NonText ctermbg=none
hi LineNr ctermbg=none

" Inden Guides colors
" Toggle Indent Guides with ',+ig'
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=darkgrey
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=grey

" Highlight extra white spaces
match ExtraWhitespace /\s\+$/
au BufWinEnter * match ExtraWhitespace /\s\+$/
au BufWinLeave * call clearmatches()

"" CUSTOMIZATIONS =============================================

" set , as mapleader
let mapleader = ","

" Easy window split
nmap <leader>sh  :topleft  vnew<CR>
nmap <leader>sl  :botright vnew<CR>
nmap <leader>sk  :topleft  new<CR>
nmap <leader>sj  :botright new<CR>

" Easy window navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Easy window positioning
nnoremap <leader>h <C-w>H
nnoremap <leader>j <C-w>J
nnoremap <leader>k <C-w>K
nnoremap <leader>l <C-w>L

" Tab navigation
nnoremap th :tabfirst<CR>
nnoremap tj :tabprev<CR>
nnoremap tk :tabnext<CR>
nnoremap tl :tablast<CR>
nnoremap tn :tabnew<CR>
nnoremap td :tabclose<CR>
nnoremap tt :tabedit<CR>

" Paste in a new line
nnoremap <C-p> o <C-r>"<ESC>

" Ctrl-N to disable search match highlight
nmap <silent> <C-N> :silent noh<CR>

" Space to toggle folds.
nnoremap <Space> za
vnoremap <Space> za

" NerdTree config
"let g:NERDTreeShowHidden = 0
"let g:NERDTreeMinimalUI = 0
"let g:NERDTreeIgnore = []
"let g:NERDTreeStatusline = ''
"let g:NERDTreeDirArrowExpandable = '+'
"let g:NERDTreeDirArrowCollapsible = '-'
"noremap <leader>f :NERDTreeToggle<CR>

"NeoTree config
nnoremap <silent> <leader>f :Neotree action=focus toggle=true position=left<cr>

" Use jj to escape from insert mode
inoremap jj <ESC>

" Use x11 clipboard (neovim)
function! ClipboardYank()
    call system('xclip -i -selection clipboard', @@)
endfunction
function! ClipboardPaste()
    call system('xclip -o -selection clipboard')
endfunction
noremap <leader>y y:call ClipboardYank()<CR>
noremap <leader>p p:call ClipboardPaste()<CR>
noremap <leader>Y "*y
noremap <leader>P "*p

" On file types...
"   .md files are markdown files
autocmd BufNewFile,BufRead *.md     setlocal ft=markdown
"   .twig files use html syntax
autocmd BufNewFile,BufRead *.twig   setlocal ft=html
"   .jinja2 files use html syntax
autocmd BufNewFile,BufRead *.jinja2 setlocal ft=html
"   .less files use less syntax
autocmd BufNewFile,BufRead *.less   setlocal ft=less
"   .jade files use jade syntax
autocmd BufNewFile,BufRead *.jade   setlocal ft=jade
"   .ts and .tsx files use typescript syntax
autocmd BufNewFile,BufRead *.ts     setlocal ft=typescriptreact
autocmd BufNewFile,BufRead *.tsx    setlocal ft=typescriptreact

" Format json files
com! FormatJSON %!python3 -m json.tool
" Disable quote concealing in JSON files
let g:vim_json_conceal=0


" start autocompletition
" set omnifunc=syntaxcomplete#Complete

" Show cool status line on one screen as well
set laststatus=2
" to get the right separators
set encoding=utf-8
scriptencoding utf-8

" STATUS LIGHTLINE SETTINGS =================
" Tab styling
let g:airline#extensions#tabline#enabled = 0
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:airline#extensions#tabline#left_alt_sep = '|'

" Status bar styling
let g:airline#extensions#ale#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_theme = 'codedark'
let g:Powerline_symbols = 'fancy'
let g:airline_left_sep = "\uE0B0"
let g:airline_right_sep = "\uE0B2"
let g:airline_left_alt_sep = "\uE0B1"
let g:airline_right_alt_sep = "\uE0B3"
function! AirlineInit()
    let g:airline_section_a = airline#section#create(['mode'])
    "let g:airline_section_b = airline#section#create_left(['%{ strftime("%c") }'])
    let g:airline_section_b = airline#section#create_left(['branch'])
endfunction
autocmd VimEnter * call AirlineInit()

" Use TAB to complete when typing words, else inserts TABs as usual.
" Uses dictionary and source files to find matching words to complete.
" "Note: usual completion is on <C-n> but more trouble to press all the time.
" Never type the same word twice and maybe learn a new spellings!
" Use the Linux dictionary when spelling is in doubt.
function! Tab_Or_Complete()
    if col('.')>1 && strpart( getline('.'), col('.')-2, 3 ) =~ '^\w'
        return "\<C-N>"
    else
        return "\<Tab>"
    endif
endfunction
:inoremap <Tab> <C-R>=Tab_Or_Complete()<CR>
:set dictionary="/usr/dict/words"

" Indent lines
let g:indentLine_char = '|'
" Colors for tsx ts jsx
let g:vim_jsx_pretty_colorful_config = 1 " default 0

" Conquer Of Completion (COC) config ==================================
" COC Packages
let g:coc_node_path = '/home/zeek/.nvm/versions/node/v16.15.0/bin/node'
let g:coc_disable_transparent_cursor = 1
let g:coc_global_extensions = [
    \ 'coc-json',
    \ 'coc-css',
    \ 'coc-pyright',
    \ 'coc-eslint',
    \ 'coc-emmet',
    \ 'coc-tsserver',
    \ 'coc-flow',
    \ 'coc-toml',
    \ 'coc-yaml',
    \ 'coc-markdownlint',
    \ 'coc-markdown-preview-enhanced',
    \ 'coc-snippets',
    \ 'coc-vimlsp',
    \ ]

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("nvim-0.5.0") || has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ CheckBackspace() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call ShowDocumentation()<CR>

function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')
" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)
" Formatting selected code.
"xmap <leader>ff  <Plug>(coc-format-selected)
"nmap <leader>ff  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Run the Code Lens action on the current line.
nmap <leader>cl  <Plug>(coc-codelens-action)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocActionAsync('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR :call CocActionAsync('runCommand', 'editor.action.organizeImport')

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>
=======
" Jump to method definitions
"nnoremap <leader>d :call CocAction('jumpDefinition', v:false)<CR>
"nnoremap <leader>ds :call CocAction('jumpDefinition', 'split')<CR>
nnoremap <leader>d :call CocAction('doHover')<CR>
"nnoremap <leader>d :call CocAction('jumpDefinition', 'vsplit')<CR>
nnoremap <leader>dt :call CocAction('jumpDefinition', 'tabe')<CR>
>>>>>>> 8236bb63922bb31b531f319b46801f014c3f2075

"" PLUGINS ====================================================
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()

"Plug 'scrooloose/nerdtree'             " NerdTree bar on the side to open files
Plug 'vim-airline/vim-airline'         " Cool status bar
Plug 'vim-airline/vim-airline-themes'  " Status bar themes
Plug 'tpope/vim-fugitive'              " Add git branch to status bar
Plug 'yggdroot/indentline'             " Indent lines
Plug 'tomasiser/vim-code-dark'         " vscode dark colors
Plug 'pangloss/vim-javascript'         " Javascript and Typescript support
Plug 'maxmellon/vim-jsx-pretty'        " jsx colors and tsx
Plug 'cespare/vim-toml', { 'branch': 'main' }    " Syntax highlight toml files
Plug 'neoclide/coc.nvim', {'branch': 'release'}  " Syntax checking with linting
Plug 'jparise/vim-graphql'             " graphql support
Plug 'ryanoasis/vim-devicons'          " dev icons for nerdtree

call plug#end()

lua require('plugins')
